import java.util.*;

public class Main
{
	public static void main(String[] args) {
	     
	    //Lab 7 - Sum of elements of array
	     
	    //array declaration
	    int[] arr = new int[8];
	    
	    Scanner sc = new Scanner(System.in);
	    
	    //Taking inpur array
	    System.out.println("Enter 8 integers");
	    for(int i=0; i<8; i++){
	        arr[i] = sc.nextInt();
	    }
	    
	    //Find Sum
	    int sum=0;
	    for(int i=0; i<8;i++){
	        sum=sum+arr[i];
	    }
	    System.out.println("Sum is: "+sum);
	}
}
