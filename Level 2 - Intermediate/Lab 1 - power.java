import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 1 - Power of a number using loop
		
		//variable declaration and initialization
		int number, exponent, i;
		long result=1;
		
		//Taking input n
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the value of number");
		number = sc.nextInt();
		
		System.out.println("Enter the value of power");
		exponent = sc.nextInt();
		
		for(i=1; i<=exponent; i++){
		    result = result*number;
		}
		System.out.println(result);
		
		
	}
}
