import java.util.*;

public class Main
{
	public static void main(String[] args) {
	    
	    //Lab 2 - Binary search
	    
	    //Variable declaration;
	    int n,search;
	    
		Scanner sc = new Scanner(System.in);
		
		//Taking input n
		System.out.println("Enter number of integers");
		n = sc.nextInt();
		
		//Array declaration
		int[] arr = new int[n];
		
		System.out.println("Enter the integers");
		for(int i=0; i<n; i++){
		    arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter the integer to be searched");
		search = sc.nextInt();
		
		for(int i=0; i<n; i++){
		    if(arr[i]==search){
		        System.out.println(search+" found at position "+i);
		        break;
		    }
		}
	}
}
