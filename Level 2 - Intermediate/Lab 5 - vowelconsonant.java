import java.util.*;

public class Main
{
	public static void main(String[] args) {
	     
	    //Lab 5 - vowel or consonant
	     
	    //Variable declaration
	    char c;
	    boolean isVowel = false;
	    
	    Scanner sc = new Scanner(System.in);
	    
	    //Taking inpur Character
	    System.out.println("Enter a letter");
	    c = sc.next().charAt(0);
	    
	    //switch case to check vowel or consonant
		switch(c){
		    case 'a': isVowel=true;
		                break;
    		case 'e': isVowel=true;
    		            break;
		    case 'i': isVowel=true;
		                break;
    		case 'o': isVowel=true;
    		            break;
		    case 'u': isVowel=true;
		                break;
		    case 'A': isVowel=true;
		                break;
		    case 'E': isVowel=true;
		                break;
		    case 'I': isVowel=true;
		                break;
		    case 'O': isVowel=true;
		                break;
		    case 'U': isVowel=true;
		                break;
		    default: isVowel=false;
		                break;
		}
		if(isVowel==true){
		    System.out.println("Entered character is a vowel");
		}
		else{
		    System.out.println("Entered character is a consonant");
		}
	}
}
