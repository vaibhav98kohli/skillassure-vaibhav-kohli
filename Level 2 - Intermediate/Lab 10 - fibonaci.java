import java.util.*;

public class Main
{
	public static void main(String[] args) {
	
		// Lab - 10 Fibonacci series
		
		//Variable declaration
	    int n1=0,n2=1,n3,i,n;
	    
	    //Taking input
	    Scanner sc = new Scanner(System.in);
	    System.out.println("Enter the value of n");
	    n = sc.nextInt();
	    
	    //printing 0 and 1
        System.out.print(n1+" "+n2);    
           
        //printing rest of the series 
        for(i=2;i<n;++i)    
        {    
            n3=n1+n2;    
            System.out.print(" "+n3);    
            n1=n2;    
            n2=n3;    
        }      
		
	}
}
