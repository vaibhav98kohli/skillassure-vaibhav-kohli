import java.util.*;

public class Main
{
	public static void main(String[] args) {
	     
	    //Lab 4 - Prime numbers 1-100
	     
	    //Variable declaration
	    int counter;
	    String primeNumbers ="";
	    
		for(int i=1; i<=100; i++){
		    counter =0;
		    for(int j=i; j>=1; j--){
		        if(i%j==0){
		            counter++;
		        }
		    }
		    if(counter==2){
		        primeNumbers += i+" ";
		    }
		}
		System.out.println(primeNumbers);
	}
}
