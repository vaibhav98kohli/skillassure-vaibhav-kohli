import java.util.*;

public class Main
{
	public static void main(String[] args) {
	    
	    //Lab 3 - Reverse a number
	    
	    //Variable declaration;
	    int number, digit, reversedNumber=0;
	    
		Scanner sc = new Scanner(System.in);
		
		//Taking input number
		System.out.println("Enter the number to be reversed");
		number = sc.nextInt();
		
		while(number>0){
		  digit = number%10;
		  reversedNumber = reversedNumber*10 + digit;
		  number=number/10;
		}
		
		System.out.println("Reversed number is: "+reversedNumber);
	}
}
