import java.util.*;

public class Main
{
    
    // Lab 2 - Linear search
    
	public static void main(String[] args) {
		
		int n;
		int pos=-1;
		
		Scanner sc = new Scanner(System.in);
		
		//Taking input n
		System.out.println("Enter total number of elements");
		n = sc.nextInt();
		
		int[] arr = new int[n];
		
		//Taking input integers
		System.out.println("Enter the elements");
		for(int i=0; i<n;i++){
		    arr[i]= sc.nextInt();
		}
		
		//Take input of element to be searched
		System.out.println("Enter the element to be searched");
		int search = sc.nextInt();
		
		//Begin search
		for(int i=0; i<n; i++){
		    if (arr[i]==search){
		        pos=i;
		        break;
		    }
		}
		
	    if(pos==-1){
	        System.out.println("Element not found");
	    }else{
	        System.out.println("Element Found at position "+pos);
	    }
	}
}
