import java.util.*;

public class Main
{
    
    // Lab 4
    
	public static void main(String[] args) {
		
		String name="Vaibhav Kohli";
		int empId=110011;
		int basic=50000;
		int specialAllowance=8000;
		int bonusPercent=12;
		int taxSavingInvestment=2000;
		int grossMonthlySalary = basic+specialAllowance;
		int tax=0;
		int finalTax=0;
		
		int annualSalary = (grossMonthlySalary*12);
		int annualGrossSalary = (grossMonthlySalary*12) + ((bonusPercent/100)*annualSalary);
		int annualNetSalary;
		
		if(annualGrossSalary<250000){
		    tax=0;
		}
		else if(annualGrossSalary>=250000 && annualGrossSalary<500000){
		    tax = (5/100)*annualGrossSalary;
		}
		else if(annualGrossSalary>=500000 && annualGrossSalary<1000000){
		    tax = (20/100)*annualGrossSalary;
		}
		else if(annualGrossSalary>=1000000){
		    tax = (30/100)*annualGrossSalary;
		}
		
		finalTax = tax-taxSavingInvestment;
		
		annualNetSalary = annualGrossSalary-finalTax;
		
		System.out.println("Annual Gross Salary= "+annualGrossSalary);
		System.out.println("Annual Net Salary= "+annualNetSalary);
		System.out.println("Tax Payable= "+finalTax);
		
	
	}
}
