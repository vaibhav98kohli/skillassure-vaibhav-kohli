import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 6(b) - series
		
		//variable declaration and initialization
		int i=4, j, n;
		
		//Taking input n
		System.out.println("Enter value of n");
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		
	
		//Series logic and printing
		for(j=1; j<=n; j++){
		    System.out.println(i*j*j);
		}
	}
}
