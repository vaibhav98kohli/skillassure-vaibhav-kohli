import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 7 - Print words in a different line
		
		//variable declaration and initialization
		String s = "This is a beautiful world";
		
		//split the string and store in array
		String[] s2 = s.split(" ");
		
		//print the array in different lines
		for(int i=0; i<s2.length; i++){
		    System.out.println(s2[i]+"\n");
		}
	}
}
