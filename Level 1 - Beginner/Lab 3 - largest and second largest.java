import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 3 - largest and second largest number
		
		//variable declaration and initialization
		int num1, num2, num3;
		
		
		//Scanner for input
		Scanner sc = new Scanner(System.in);
		
		//Accept Input
		System.out.println("Enter number 1");
		num1 = sc.nextInt();
		
		System.out.println("Enter number 2");
		num2 = sc.nextInt();
		
		System.out.println("Enter number 3");
		num3 = sc.nextInt();
		
		//Find largest number
		if(num1>num2){
		    if(num1>num3){
		        System.out.println(num1+" is largest");
		    }
		    else{
		        System.out.println(num3+" is largest");
		    }
		}
		else{
		    if(num2>num3){
		        System.out.println(num2+" is largest");
		    }
		    else{
		        System.out.println(num3+" is largest");
		    }
		}
		
		//Find 2nd largest number
		if(num1>num2 && num1<num3 || num1<num2 && num1>num3){
		    System.out.println(num1+" is 2nd largest");
		}
		else if(num2>num1 && num2<num3 || num2>num3 && num2<num1){
		    System.out.println(num2+" is 2nd largest");
		}
		else{
		    System.out.println(num3+" is second largest");
		}
	}
}
