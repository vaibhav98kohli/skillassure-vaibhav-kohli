import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 6(c) - series
		
		//variable declaration and initialization
		int a=1, b=1, c, n;
		
		//Taking input n
		System.out.println("Enter the value of n");
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		
		System.out.print(a+" ");
		System.out.print(b+" ");
		
		for(int i=0; i<n; i++){
		    c=a+b;
		    System.out.print(c+" ");
		    a=b;
		    b=c;
		}
		
	}
}
