import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 6(a) - series
		
		//variable declaration and initialization
		int start=1, sign=1, n;
		
		//Taking input n
		System.out.println("Enter value of n");
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		
		//Series logic and printing
		for(int i=1;i<n;i++)
        {
            System.out.println(start);
            sign=sign*(-1);
            start=(sign*i*i)-start;
        }
	}
}
