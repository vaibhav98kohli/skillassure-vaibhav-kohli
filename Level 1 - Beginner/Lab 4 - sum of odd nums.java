import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 4 - Sum of all odd numbers from 1 to n.
		
		//variable declaration and initialization
		int n, sum=0, i=1;
		
		//Scanner for input
		Scanner sc = new Scanner(System.in);
		
		//Accept Input
		System.out.println("Enter value of n");
		n = sc.nextInt();
		
		//Find sum from 1 to n.
	    while(i<=n){
	        sum=sum+i;
	        i++;
	    }
	    
	    System.out.println(sum);
	    
	}
}
