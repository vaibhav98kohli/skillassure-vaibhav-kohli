import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 5 - sum of all prime numbers from m to n
		
		//variable declaration and initialization
		int m,n,sum=0, i,j;
		
		//Scanner for input
		Scanner sc = new Scanner(System.in);
		
		//Accept Input
		System.out.println("Enter value of m");
		m = sc.nextInt();
		
		System.out.println("Enter value of m");
		n = sc.nextInt();
		
		//Find sum from m to n.
		System.out.println("Prime numbers between "+m+" and "+n+" are: ");
	    for(i=m; i<=n; i++){
	        for(j=2; j<i; j++){
	            if(i%j==0){
	                break;
	            }
	        }
	        if(j==i){
	            System.out.println(i);
	            sum=sum+i;
	        }
	    }
	    System.out.println("Sum of prime numbers between "+m+" and "+n+" is: ");
	    System.out.println(sum);
	    
	}
}
