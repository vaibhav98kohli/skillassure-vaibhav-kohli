import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// 2) Even / Odd
		
		//variable declaration and initialization
		int num;
		
		//Scanner for input
		Scanner sc = new Scanner(System.in);
		
		//Accept Input
		System.out.println("Enter number");
		num = sc.nextInt();
		
		//Check even or odd and print the result
        if(num%2==0){
            System.out.println(num+" is even");
        }		
        else{
            System.out.println(num+" is odd");
        }

	}
}
