public class Main
{
	public static void main(String[] args) {
	
		// Lab - 8 Reverse the case of string
		
		//declare variables
		String str = "heLLo WoRld";
		StringBuffer str1 = new StringBuffer(str);
		
		//reverse the case of string
		for(int i=0; i<str.length(); i++){
		    if(Character.isLowerCase(str.charAt(i))){
		        str1.setCharAt(i, Character.toUpperCase(str.charAt(i)));
		    }
		    else if(Character.isUpperCase(str.charAt(i))){
		        str1.setCharAt(i, Character.toLowerCase(str.charAt(i)));
		    }
		}
		//print final string
		System.out.println(str1);
	}
}
