import java.util.*;

public class Main
{
	public static void main(String[] args) {
		
		// Lab 2 - swap numbers
		
		//variable declaration and initialization
		int num1, num2;
		int temp;
		
		//Scanner for input
		Scanner sc = new Scanner(System.in);
		
		//Accept Input
		System.out.println("Enter number 1");
		num1 = sc.nextInt();
		
		System.out.println("Enter number 2");
		num2 = sc.nextInt();
		
		//Print the numbers before swapping
		System.out.println(num1 + " " +num2);
		
		//process to swap
		temp=num1;
		num1=num2;
		num2=temp;
		
		//print after swapping
		System.out.println(num1 + " " +num2);
		
	}
}
